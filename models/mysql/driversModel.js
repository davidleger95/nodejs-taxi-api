const db = require('../../config/mysqlDBConfig')

class DriverModel {

    constructor(_row) {
        this.row = _row
    }

    get fullname() {
        return this.row.fullname
    }
    
    set fullname(_fullname) {
        this.row.fullname = _fullname
    }

    get age() {
        return this.row.age
    }
    
    set age(_age) {
        this.row.age = age
    }

    get license() {
        return this.row.license
    }
    
    set license(_licence) {
        this.row.license = _licence
    }

    get phone() {
        return this.row.phone
    }
    
    set phone(_phone) {
        this.row.phone = _phone
    }

    get address() {
        return this.row.address
    }
    
    set address(_address) {
        this.row.address = _address
    }

    get createdAt() {
        return this.row.created_at
    }
    
    set createdAt(_created_at) {
        this.row.created_at = _created_at
    }

    /**
     * Store a driver
     * @param {*} fullname 
     * @param {*} age 
     * @param {*} license 
     * @param {*} phone 
     * @param {*} address 
     * @param {*} callback 
     */
    static storeOne(fullname, age, license, phone, address, callback) {

        db.query(
            'INSERT INTO drivers(fullname, age, license, phone, address, created_at) VALUES(?, ?, ?, ?, ?, NOW())', 
            [fullname, age, license, phone, address], 
            (err) => {
                
                if (err) throw "Storing error " + err
                callback()
                // console.log("User added");
            }
        )

    }

    /**
     * Find all driver order by descendent creation date
     * @param {*} callback 
     */
    static findAll(callback) {
        
        db.query(
            'SELECT * FROM drivers ORDER BY created_at DESC', 
            (err, rows) => {

                if (err) throw "Database error : " + err

                callback(rows.map( (row) => new DriverModel(row) ))
            }
        )

    }
    /**
     * Find a specific and only one driver
     * @param {*} id 
     * @param {*} callback 
     */
    static findOne(id, callback) {

        db.query('SELECT * FROM drivers WHERE id = ? LIMIT 1', [id], (err, row) => {

            if (err) throw "Database error : " + err
            callback(row)
            // callback(new DriverModel(row[0]))

        })
    }

    /**
     * Update a specific and one driver
     * @param {*} id 
     * @param {*} fullname 
     * @param {*} age 
     * @param {*} license 
     * @param {*} phone 
     * @param {*} address 
     * @param {*} callback 
     */
    static updateOne(id, fullname, age, license, phone, address, callback) {

        db.query('UPDATE drivers SET fullname = ?, age = ?, license = ?, phone = ?, address =? WHERE id = ?', [fullname, age, license, phone, address, id], (err) => {
            
            if (err) throw "Updating error : " + err
            callback()
            // console.log("Driver with ID : " + id + "was updated")
        })
    }

    /**
     * Delete a specific driver
     * @param {*} id 
     * @param {*} callback 
     */
    static deleteOne(id, callback) {
        db.query('DELETE FROM drivers WHERE id = ?', [id], (err) => {
            if (err) throw "Deleting error : " + err
            callback()
            // console.log("Driver with ID : " + id + "was deleted");
        })
    }

}

// Export class driver model
module.exports = { DriverModel }