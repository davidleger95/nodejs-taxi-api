/**
 * Include all required module
 * [
 *  express
 *  body-parser
 * ]
 */
const express = require('express')
const app = express()
const bodyParser = require('body-parser')

// Load mongoDB configuration
require('./config/mongoDBConfig')


/**
 * Include all routes|controllers
 */
const driverRoutes = require('./routes/mongodb/driversController')
const carRoutes = require('./routes/mongodb/carsController')

/**
 * Load all middleware
 */
app.use(bodyParser.json())
app.use('/api/v0.1/drivers', driverRoutes)
app.use('/api/v0.1/cars', carRoutes)


/**
 * Run server in port 8080
 */
app.listen(8080, () => {
    console.log('Server started in 8080')
})