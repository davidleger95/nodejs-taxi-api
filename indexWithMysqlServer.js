/**
 * Include all required module
 * [
 *  express
 *  body-parser
 * ]
 */
const express = require('express')
const app = express()
const bodyParser = require('body-parser')

// Load MySQL configuration
const db = require('./config/mysqlDBConfig')

// Include driver route
const driverRoutes = require('./routes/mysql/driversController')

/**
 * Load all middleware
 */
app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({ extended: true }))
app.use('/api/v0.1/drivers', driverRoutes)
// app.use('/api/v0.1/cars', carRoutes)

/**
 * Run server in port 8080
 */
app.listen(8080, () => {
    console.log('Server started in 8080')
})