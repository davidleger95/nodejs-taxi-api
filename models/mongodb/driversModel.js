const mongoose = require('mongoose')

/**
 * Driver schema which content all field
 */
const DriverSchema = mongoose.Schema({

    fullname: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    license: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    }

})

const DriversModel = mongoose.model(
    // Model name
    'taxi_api_driver_model',
    // Driver model schema
    DriverSchema,
    // Collection name
    'drivers'
)

// Export driver model
module.exports = { DriversModel }