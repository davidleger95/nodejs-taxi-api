const mongooseDb = require('mongoose')

/**
 * Connect to the mongodb server and the taxi_api database
 */
mongooseDb.connect(
    'mongodb://localhost:27017/taxi_api',
    { useNewUrlParser: true, useUnifiedTopology: true},
    (err) => {
        if (!err) console.log('MongoDB connected');
        else  console.error('MongoDB connection error : ' + err);
    }
)