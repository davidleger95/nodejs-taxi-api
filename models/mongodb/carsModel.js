const mongoose = require('mongoose')

/**
 * Car schema which content all field
 */
const CarsSchema = mongoose.Schema({
    
    number : {
        type : String,
        required : true
    },
    year: {
        type: Number,
        required: true 
    },
    mark: {
        type: String,
        required: true
    },
    carburator: {
        type: String,
        required: true
    },
    declared_at: {
        type: Date,
        default: Date.now
    },
    driver_id: {
        type: String,
        required: true
    }
})

const CarsModel = mongoose.model(
    // model name
    'taxi_api_car_model', 
    // Car model schema
    CarsSchema, 
    'cars'
    // Collection name
    )

// Export car model
module.exports = { CarsModel }