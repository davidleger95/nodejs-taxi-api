const express = require('express')
const router = express.Router()

// Include model
const { DriverModel } = require('../../models/mysql/driversModel')

/**
 * Display all driver
 * @method(GET)
 */
router.get('/', (req, res) => {

    DriverModel.findAll((drivers) => {
        res.status(200).send(drivers)
    })
})

/**
 * Store a new driver
 * @method(POST)
 */
router.post('/', (req, res) => {

    DriverModel.storeOne(req.body.fullname, req.body.age, req.body.license, req.body.phone, req.body.address, (storedDriver) => {
        res.status(200).redirect("/")
    })
})

/**
 * Find a specific and only one driver
 * @param(id)
 * @method(GET)
 */
router.get('/:id', (req, res) => {
    DriverModel.findOne(parseInt(req.params.id), (driver) => {
        res.status(200).send(driver)
    })
})

/**
 * Update a specific driver 
 * @param(id)
 * @method(PUT)
 */
router.put('/:id', (req, res) => {

    DriverModel.updateOne(req.params.id, req.body.fullname, req.body.age, req.body.license, req.body.phone, req.body.address, () => {
        res.status(200).redirect("/" + req.params.id)
    })
})

/**
 * Delete a specific driver
 * @param(id)
 * @method(DELETE)
 */
router.delete('/:id', (req, res) => {
    DriverModel.deleteOne(req.params.id, () => {
        res.status(200).redirect('/')
    })
})

// Export all routes
module.exports = router