const express = require('express')
const router = express.Router()

// Verify if OID has ObjectId type
const OID = require('mongoose').Types.ObjectId

/**
 * Include mongodb car model
 */
const { CarsModel } = require('../../models/mongodb/carsModel')

/**
 * Display all car
 * @method(GET)
 */
router.get('/', (req, res) => {

    CarsModel.find((err, docs) => {

        if (err) console.error('Cars document error' + err)
        res.status(200).send(docs)

    })
})

/**
 * Store a car information
 * @method(POST)
 */
router.post('/', (req, res) => {

    if (!OID.isValid(req.body.driver_id)) {
        return res.status(401).send("Unknown car ID: " + req.params.id)
    }

    const newCar = new CarsModel({
        number: req.body.number,
        year: req.body.year,
        mark: req.body.mark,
        carburator: req.body.carburator,
        driver_id: req.body.driver_id
    })

    newCar.save((err, doc) => {
        if (err) console.error("storing error " + err)
        res.status(200).send(doc)
    })
})


/**
 * Show a specific car information
 * @param(id)
 * @method(GET)
 */
router.get('/:id', (req, res) => {

    if (!OID.isValid(req.params.id)) {
        return res.status(404).send("Unknown car ID: " + req.params.id)
    }

    CarsModel.findById(req.params.id, (err, doc) => {
        res.status(200).send(doc)
    }).limit(1)
    
})

/**
 * Update a specific car information
 * @param(id)
 * @method(PUT)
 */
router.put('/:id', (req, res) => {

    if (!OID.isValid(req.params.id)) {
        return res.status(401).send("Unknown car ID: " + req.params.id)
    }

    const newCarInformation = {

        number: req.body.number,
        year: req.body.year,
        mark: req.body.mark,
        carburator: req.body.carburator,
        driver_id: req.body.driver_id

    }

    CarsModel.findByIdAndUpdate(
        req.params.id,
        { $set: newCarInformation },
        { new: true },
        (err, doc) => {
            if (err) console.error("Update error " + err)
            res.status(200).send(doc)
        }
    )
})

/**
 * Delete a specific car
 * @param(id)
 * @method(DELETE)
 */
 router.delete('/:id', (req, res) => {

    if (!OID.isValid(req.params.id)) {
        return res.status(401).send("Unknown car ID: " + req.params.id)
    }

    CarsModel.findOneAndDelete(req.params.id, (err, doc) => {
        if (err) console.error("Deleting erro " + err)
        console.log("Document deleted")
        res.status(301).redirect('/')
    })

 })

// Export all routes
module.exports = router