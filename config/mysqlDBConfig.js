// Include mysql driver
const mysql = require('mysql')

/**
 * Create a mysql connecion:
 * in localhost
 * with root user and empty password
 * and taxi_api database
 */
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'taxi_api'
})

// Connect to the database
connection.connect((err) => {
    if (err) {
        console.error("Mysql connection error" + err.stack)
        return;
    }

    console.log("Mysql connected with ID: " + connection.threadId);

})

// Export connection
module.exports = connection