const express = require('express')
const router = express.Router()

// Assign an ObjectId type to OID
const OID = require('mongoose').Types.ObjectId

// Require mongodb driver model
const { DriversModel } = require('../../models/mongodb/driversModel')

/**
 * Display all driver 
 * @method(GET)
 */
router.get('/', (req, res) => {

    DriversModel.find((err, docs) => {

        if (err) console.error('Driver document error' + err)
        res.status(200).send(docs)
    })
})

/**
 * Store a new driver information
 * @method(POST)
 */
router.post('/', (req, res) => {

    const newDriver = new DriversModel({

        fullname: req.body.fullname,
        age: req.body.age,
        permit: req.body.permit,
        phone: req.body.phone,
        address: req.body.address
    })

    newDriver.save((err, docs) => {

        if(err) console.error("Saving driver error" + err)
        res.status(200).send(docs)

    })
})


/**
 * Update information of a specific driver
 * @param(id)
 * @method(PUT)
 */
router.put('/:id', (req, res) => {

    // Verify if OID has ObjectId type (ex : 6030c581037abc501f5dbdd2)
    if (!OID.isValid(req.params.id)) {
        return res.status(401).send("Unknown ID" + (req.params.id))
    }

    const newDriverInformation = {
        fullname: req.body.fullname,
        age: req.body.age,
        permit: req.body.permit,
        phone: req.body.phone,
        address: req.body.address
    }

    DriversModel.findByIdAndUpdate(
        req.params.id,
        { $set: newDriverInformation },
        { new: true },
        (err, docs) => {
            if (err) console.error("Update document error " +  err)
            res.status(200).send(docs)
        }
    )


})

/**
 * Delete a specific driver
 * @param(id)
 * @method(DELETE)
 */
router.delete('/:id', (req, res) => {

    // Verify if OID has ObjectId type (ex : 6030c581037abc501f5dbdd2)
    if (!OID.isValid(req.params.id)) {
        return res.status(401).send("Unknown ID" + (req.params.id))
    }

    DriversModel.findByIdAndDelete(req.params.id, (err, doc) => {
        if (err) console.error("Deleting error " + err)
        console.log("Document deleted")
        res.redirect('/')
    })
})

/**
 * Show a specific driver information
 * @param(id)
 * @method(GET)
 */
router.get('/:id', (req, res) => {

    // Verify if OID has ObjectId type (ex : 6030c581037abc501f5dbdd2)
    if (!OID.isValid(req.params.id)) {
        return res.status(404).send("Unknown ID" + (req.params.id))
    }

    // Find the first document by its ID
    DriversModel.findById(req.params.id, (err, doc) => {
        if (err) console.error("Unknown document " + err)
        res.status(200).send(doc)
    }).limit(1)
})

// Export all car routes
module.exports = router